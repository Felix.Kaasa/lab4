package datastructure;

import cellular.CellState;

import java.util.ArrayList;
import java.util.List;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState initialState;
    private List<CellState> grid = new ArrayList<>(rows*cols);

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        this.initialState = initialState;
        for (int i = 0; i < rows * columns; i++) {
            grid.add(i, initialState);
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        outOfBounds(row, column);
        int index = coordinatesToIndex(row, column);
        grid.set(index, element);
        
    }

    @Override
    public CellState get(int row, int column) {
        outOfBounds(row, column);
        int index = coordinatesToIndex(row, column);
        return grid.get(index);
    }

    @Override
    public IGrid copy() {
        IGrid copy = new CellGrid(this.rows, this.cols, this.initialState);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                copy.set(i, j, this.get(i, j));
            }
        }
        return copy;
    }

    private int coordinatesToIndex(int x, int y){
        return x + numRows() * y;
    }

    private void outOfBounds(int row, int column){
        if (row > this.rows || row < 0){
            throw new IndexOutOfBoundsException("The row is out of bounds.");
        }
        if (column > this.cols || column < 0){
            throw new IndexOutOfBoundsException("The column is out of bounds.");
        }
    }
    
}
